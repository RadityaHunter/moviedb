package com.adit.movie.utils

data class BasicResponse<T>(
    val id: Int,
    val page: Int,
    val results: ArrayList<T>
)
