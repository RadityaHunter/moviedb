package com.adit.movie.utils

import java.text.SimpleDateFormat
import java.util.*

object DateHelper {
    fun getCurrentDate(date: String) : String{
        val inputDate = SimpleDateFormat("yyyy-MM-dd", Locale.US)
        val outputDate = SimpleDateFormat("MMM dd, yyyy", Locale.US)
        val dates = inputDate.parse(date)
        return outputDate.format(dates!!)
    }
}