package com.adit.movie.network

import com.adit.movie.model.ModelMoviePopular
import com.adit.movie.model.ResponseReview
import com.adit.movie.utils.BasicResponse
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface ApiInterface {
    @GET("movie/popular")
    fun getListMovie(@Query("api_key") apiKey: String): retrofit2.Call<BasicResponse<ModelMoviePopular>>

    @GET("movie/top_rated")
    fun getListMovieTopRated(@Query("api_key") api_key: String): retrofit2.Call<BasicResponse<ModelMoviePopular>>

    @GET("movie/now_playing")
    fun getListMovieNowPlaying(@Query("api_key") api_key: String): retrofit2.Call<BasicResponse<ModelMoviePopular>>

    @GET("movie/{movie_id}")
    fun getDetailMovie(
        @Path("movie_id") id: Int,
        @Query("api_key") api_key: String
    ): retrofit2.Call<ModelMoviePopular>

     @GET("movie/{movie_id}/reviews")
     fun getMovieReviews(
         @Path("movie_id") id: Int,
         @Query("api_key") api_key: String
     ): retrofit2.Call<ResponseReview>



}