package com.adit.movie.model

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.parcelize.Parcelize

data class ResponseReview(
    @SerializedName("id") var movieId:Int?=0,
    @SerializedName("results") var result: ArrayList<ModelReview>
)

@Parcelize
data class ModelReview(
    @SerializedName("content") var content:String,
    @SerializedName("author") var author:String,
    @SerializedName("created_at") var created_at:String
) : Parcelable
