package com.adit.movie.model

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.parcelize.Parcelize

@Parcelize
data class ModelMoviePopular(
    @SerializedName("id")
    var id: Int?=0,
    @SerializedName("title")
    var title: String? = null,
    @SerializedName("original_title")
    var originalTitle: String? = null,
    @SerializedName("poster_path")
    var poster: String? = null,
    @SerializedName("backdrop_path")
    var posterBackdrop: String? = null,
    @SerializedName("release_date")
    var release: String? = null,
    @SerializedName("overview")
    var overview: String? = null,
) : Parcelable