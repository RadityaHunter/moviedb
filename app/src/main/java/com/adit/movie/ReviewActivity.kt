package com.adit.movie

import android.os.Bundle
import android.widget.Toast
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import com.adit.movie.adapter.ReviewMovieAdapter
import com.adit.movie.databinding.ActivityReviewBinding
import com.adit.movie.model.ModelMoviePopular
import com.adit.movie.model.ModelReview
import com.adit.movie.mvvm.ReviewViewModel
import com.adit.movie.utils.Status
import com.bumptech.glide.Glide

class ReviewActivity : AppCompatActivity() {

    private lateinit var binding: ActivityReviewBinding
    private val detailViewModel: ReviewViewModel by viewModels()
    private val adapterReview: ReviewMovieAdapter = ReviewMovieAdapter()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivityReviewBinding.inflate(layoutInflater)
        setContentView(binding.root)

        setSupportActionBar(binding.toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayShowTitleEnabled(false)
        supportActionBar?.setDisplayShowHomeEnabled(true)

        val idMovie = intent.extras?.getInt(EXTRA_DATA)

        if (idMovie != null) {
            setViewModel(idMovie)
        }

        actionButton()
        getDataReview()
    }

    private fun setViewModel(movieId: Int) {
        detailViewModel.detailMovie(movieId).observe(this, {
            when (it.status) {
                Status.SUCCESS -> {
                    getDataDetail(it.data!!)
                }
                else -> {}
            }
        })

        detailViewModel.reviewMovie(movieId).observe(this, {
            when (it.status) {
                Status.SUCCESS -> {
                    adapterReview.setData(it.data!!)
                }
                else -> {}
            }
        })
    }

    private fun getDataDetail(data: ModelMoviePopular) {
        Glide.with(this)
            .load(BuildConfig.BASE_URL_IMAGE + data.posterBackdrop)
            .into(binding.posterItem)
        binding.tvTitle.text = data.title
        binding.titleToolbar.text = data.title
        binding.description.text = data.overview
    }

    private fun getDataReview() {
        binding.rvReview.layoutManager = LinearLayoutManager(this)
        binding.rvReview.setHasFixedSize(true)
        binding.rvReview.adapter = adapterReview
    }

    private fun actionButton() {
        binding.actionFavorite.setOnClickListener {
            Toast.makeText(this, "Favorite", Toast.LENGTH_LONG).show()
        }

        binding.actionShare.setOnClickListener {
            Toast.makeText(this, "Share", Toast.LENGTH_LONG).show()
        }
    }

    override fun onBackPressed() {
        this.finish()
        super.onBackPressed()
    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return super.onSupportNavigateUp()
    }

    companion object {
        const val EXTRA_DATA = "extra_data"
    }

}