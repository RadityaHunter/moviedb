package com.adit.movie.adapter

import android.content.Intent
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.adit.movie.BuildConfig
import com.adit.movie.ReviewActivity
import com.adit.movie.databinding.ItemMovieBinding
import com.adit.movie.model.ModelMoviePopular
import com.adit.movie.utils.DateHelper
import com.bumptech.glide.Glide

class MovieAdapter: RecyclerView.Adapter<MovieAdapter.ViewHolder>() {
    private val listMovie = ArrayList<ModelMoviePopular>()

    fun setData(items: ArrayList<ModelMoviePopular>){
        listMovie.clear()
        listMovie.addAll(items)
        this.notifyDataSetChanged()
    }

    class ViewHolder(private val binding: ItemMovieBinding): RecyclerView.ViewHolder(binding.root){
        fun bind(data: ModelMoviePopular){
            Glide.with(itemView)
                .load(BuildConfig.BASE_URL_IMAGE + data.poster)
                .into(binding.posterItem)

            binding.titleItem.text = data.title
            binding.releaseItem.text = DateHelper.getCurrentDate(data.release!!)

            itemView.setOnClickListener {
                val intent= Intent(itemView.context, ReviewActivity::class.java)
                intent.putExtra(ReviewActivity.EXTRA_DATA,data.id)
                itemView.context.startActivity(intent)
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(ItemMovieBinding.inflate(LayoutInflater.from(parent.context),parent,false))
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(listMovie[position])
    }

    override fun getItemCount(): Int = listMovie.size
}