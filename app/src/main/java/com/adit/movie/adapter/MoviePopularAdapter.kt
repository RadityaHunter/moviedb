package com.adit.movie.adapter

import android.content.Intent
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.adit.movie.BuildConfig
import com.adit.movie.ReviewActivity
import com.adit.movie.databinding.ItemMoviePopularBinding
import com.adit.movie.model.ModelMoviePopular
import com.bumptech.glide.Glide

class MoviePopularAdapter : RecyclerView.Adapter<MoviePopularAdapter.MovieViewHolder>() {
    private val listMoviePopular = ArrayList<ModelMoviePopular>()

    fun setData(items: ArrayList<ModelMoviePopular>) {
        listMoviePopular.clear()
        listMoviePopular.addAll(items)
        this.notifyDataSetChanged()
    }

    class MovieViewHolder(private val binding: ItemMoviePopularBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bind(data: ModelMoviePopular) {
            binding.apply {
                Glide.with(itemView)
                    .load(BuildConfig.BASE_URL_IMAGE + data.poster)
                    .into(posterItem)

                titleItem.text = data.title

                itemView.setOnClickListener {
                    val intent= Intent(itemView.context,ReviewActivity::class.java)
                    intent.putExtra(ReviewActivity.EXTRA_DATA,data.id)
                    itemView.context.startActivity(intent)
                }
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MovieViewHolder {
        return MovieViewHolder(
            ItemMoviePopularBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            )
        )
    }

    override fun onBindViewHolder(holder: MovieViewHolder, position: Int) {
        holder.bind(listMoviePopular[position])
    }

    override fun getItemCount(): Int = listMoviePopular.size


}