package com.adit.movie.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.adit.movie.R
import com.adit.movie.databinding.ItemReviewBinding
import com.adit.movie.model.ModelReview
import com.adit.movie.utils.DateHelper

class ReviewMovieAdapter: RecyclerView.Adapter<ReviewMovieAdapter.ViewHolder>() {
    private val listReview=ArrayList<ModelReview>()

    fun setData(items: ArrayList<ModelReview>){
        listReview.clear()
        listReview.addAll(items)
        this.notifyDataSetChanged()
    }

    class ViewHolder(private val binding: ItemReviewBinding): RecyclerView.ViewHolder(binding.root) {
        fun bind(data: ModelReview){
            binding.viewReview.text = data.content
            val date = DateHelper.getCurrentDate(data.created_at)
            binding.viewAuthor.text = itemView.context.getString(R.string.review_author,data.author,date)
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(ItemReviewBinding.inflate(LayoutInflater.from(parent.context),parent,false))
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(listReview[position])
    }

    override fun getItemCount(): Int = listReview.size
}