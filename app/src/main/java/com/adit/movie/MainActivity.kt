package com.adit.movie

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.LinearLayout
import androidx.activity.viewModels
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.adit.movie.adapter.MovieAdapter
import com.adit.movie.adapter.MovieNowPlayingAdapter
import com.adit.movie.adapter.MoviePopularAdapter
import com.adit.movie.databinding.ActivityMainBinding
import com.adit.movie.mvvm.MainViewModel
import com.adit.movie.utils.Resource
import com.adit.movie.utils.Status

class MainActivity : AppCompatActivity() {
    private lateinit var binding: ActivityMainBinding
    private val mainViewModel: MainViewModel by viewModels()
    private val adapter: MoviePopularAdapter = MoviePopularAdapter()
    private val adapterMovie: MovieAdapter = MovieAdapter()
    private val adapterMovieNowPlaying: MovieNowPlayingAdapter = MovieNowPlayingAdapter()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        setViewModel()
        setRecyclerViewMoviePopular()
        setRecyclerViewMovieTopRated()
        setRecyclerViewMovieNowPlaying()
    }

    private fun setViewModel(){
        mainViewModel.getAllMovie().observe(this,{
            when(it.status){
                Status.SUCCESS -> {
                    adapter.setData(it.data!!)
                }
                else -> {}
            }
        })

        mainViewModel.getAllMovieTopRated().observe(this,{
            when(it.status){
                Status.SUCCESS -> {
                    adapterMovie.setData(it.data!!)
                }
                else -> {}
            }
        })

        mainViewModel.getAllMovieNowPlaying().observe(this,{
            when(it.status){
                Status.SUCCESS -> {
                    adapterMovieNowPlaying.setData(it.data!!)
                }
                else -> {}
            }
        })
    }

    private fun setRecyclerViewMoviePopular(){
        binding.rvMoviePopular.layoutManager = LinearLayoutManager(this,LinearLayoutManager.HORIZONTAL,false)
        binding.rvMoviePopular.setHasFixedSize(true)
        binding.rvMoviePopular.adapter = adapter
    }

    private fun setRecyclerViewMovieTopRated(){
        binding.rvMovieTopRated.layoutManager = LinearLayoutManager(this,LinearLayoutManager.HORIZONTAL,false)
        binding.rvMovieTopRated.setHasFixedSize(true)
        binding.rvMovieTopRated.adapter = adapterMovie
    }

    private fun setRecyclerViewMovieNowPlaying(){
        binding.rvMovieNowPlaying.layoutManager =   LinearLayoutManager(this,LinearLayoutManager.HORIZONTAL,false)
        binding.rvMovieNowPlaying.setHasFixedSize(true)
        binding.rvMovieNowPlaying.adapter = adapterMovieNowPlaying
    }
}