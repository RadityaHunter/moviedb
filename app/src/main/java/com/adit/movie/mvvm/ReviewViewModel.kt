package com.adit.movie.mvvm

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.adit.movie.BuildConfig
import com.adit.movie.model.ModelMoviePopular
import com.adit.movie.model.ModelReview
import com.adit.movie.model.ResponseReview
import com.adit.movie.network.ApiServer
import com.adit.movie.utils.Resource
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class ReviewViewModel : ViewModel() {
    private val _reviewMovie = MutableLiveData<Resource<ArrayList<ModelReview>>>()
    private val _detailMovie = MutableLiveData<Resource<ModelMoviePopular>>()

    fun reviewMovie(id: Int): LiveData<Resource<ArrayList<ModelReview>>> {
        ApiServer.getApiService().getMovieReviews(id, BuildConfig.API_KEY)
            .enqueue(object : Callback<ResponseReview> {
                override fun onResponse(
                    call: Call<ResponseReview>,
                    response: Response<ResponseReview>
                ) {
                    if (response.isSuccessful) {
                        _reviewMovie.postValue(Resource.success(response.body()?.result))
                    }
                }

                override fun onFailure(call: Call<ResponseReview>, t: Throwable) {
                    Log.d("On Failure", t.message.toString())
                }

            })
        return _reviewMovie

    }

    fun detailMovie(id: Int): LiveData<Resource<ModelMoviePopular>> {
        ApiServer.getApiService().getDetailMovie(id, BuildConfig.API_KEY)
            .enqueue(object : Callback<ModelMoviePopular> {
                override fun onResponse(
                    call: Call<ModelMoviePopular>,
                    response: Response<ModelMoviePopular>
                ) {
                    if (response.isSuccessful) {
                        _detailMovie.postValue(Resource.success(response.body()))
                    }
                }

                override fun onFailure(call: Call<ModelMoviePopular>, t: Throwable) {
                    Log.d("On Failure", t.message.toString())
                }

            })
        return _detailMovie

    }
}