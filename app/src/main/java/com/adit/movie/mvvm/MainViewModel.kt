package com.adit.movie.mvvm

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.adit.movie.BuildConfig
import com.adit.movie.model.ModelMoviePopular
import com.adit.movie.network.ApiServer
import com.adit.movie.utils.BasicResponse
import com.adit.movie.utils.Resource
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class MainViewModel : ViewModel(){
    private val _moviePopular = MutableLiveData<Resource<ArrayList<ModelMoviePopular>>>()
    private val _movieTopRated = MutableLiveData<Resource<ArrayList<ModelMoviePopular>>>()
    private val _movieNowPlaying = MutableLiveData<Resource<ArrayList<ModelMoviePopular>>>()

    fun getAllMovie():LiveData<Resource<ArrayList<ModelMoviePopular>>> {
        ApiServer.getApiService().getListMovie(BuildConfig.API_KEY).enqueue(object: Callback<BasicResponse<ModelMoviePopular>> {
            override fun onResponse(
                call: Call<BasicResponse<ModelMoviePopular>>,
                response: Response<BasicResponse<ModelMoviePopular>>
            ) {
                if(response.isSuccessful){
                    _moviePopular.postValue(Resource.success(response.body()?.results))
                }
            }

            override fun onFailure(call: Call<BasicResponse<ModelMoviePopular>>, t: Throwable) {
                Log.d("On Failure",t.message.toString())
            }
        })
        return _moviePopular
    }

    fun getAllMovieTopRated():LiveData<Resource<ArrayList<ModelMoviePopular>>>{
        ApiServer.getApiService().getListMovieTopRated(BuildConfig.API_KEY).enqueue(object: Callback<BasicResponse<ModelMoviePopular>> {
            override fun onResponse(
                call: Call<BasicResponse<ModelMoviePopular>>,
                response: Response<BasicResponse<ModelMoviePopular>>
            ) {
                if(response.isSuccessful){
                    _movieTopRated.postValue(Resource.success(response.body()?.results))
                }
            }

            override fun onFailure(call: Call<BasicResponse<ModelMoviePopular>>, t: Throwable) {
                Log.d("On Failure",t.message.toString())
            }

        })
        return _movieTopRated
    }

    fun getAllMovieNowPlaying():LiveData<Resource<ArrayList<ModelMoviePopular>>>{
        ApiServer.getApiService().getListMovieNowPlaying(BuildConfig.API_KEY).enqueue(object: Callback<BasicResponse<ModelMoviePopular>> {
            override fun onResponse(
                call: Call<BasicResponse<ModelMoviePopular>>,
                response: Response<BasicResponse<ModelMoviePopular>>
            ) {
                if(response.isSuccessful){
                    _movieNowPlaying.postValue(Resource.success(response.body()?.results))
                }
            }

            override fun onFailure(call: Call<BasicResponse<ModelMoviePopular>>, t: Throwable) {
                Log.d("On Failure",t.message.toString())
            }

        })
        return _movieNowPlaying
    }
}